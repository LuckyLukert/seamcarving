# SeamCarving

Ein Projekt für das Juniorcamp Informatik.

https://andrewdcampbell.github.io/seam-carving


Hier zum debuggen das Beispielbild von der Tafel:
```python
img = [[[1,0,0],  [3,0,0], [5,0,0], [1,0,0]],
       [[1,0,0],  [7,0,0], [1,0,0], [2,0,0]],
       [[10,0,0], [3,0,0], [2,0,0], [5,0,0]]]
```
Als Ergebnis müsste folgendes DP-Array rauskommen:
```python
dp = [[0,0,0,0],
      [0,2,0,1],
      [5,2,1,4]]
```
