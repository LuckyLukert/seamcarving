'''input
/Users/lukert/Downloads/love.jpg
100
no-save
npfancy
'''

import numpy as np
from PIL import Image
import math

img = [[[]]]  # 2d-list of triples [x,y,z]

def h(): return len(img)
def w(): return len(img[0])


def dist():
	pass

# calculate the euclidean distance between two colors
def dist(color1, color2):
	pass

# calculate the dynamic programming array
def calcDP():
	pass

# a list of x-indices which are the seam from top to bottom with the least contrast
def getSeam():
	pass

# remove the seam from the image
def carveSeam(seam):
	pass

# remove cutAmount seams from the image
def carve(cutAmount):
	pass


if __name__ == '__main__':
	PATH = "paris.png"
	CUT_AMOUNT = 50

	original = Image.open(PATH)
	img = np.array(original, np.int32).tolist()
	carve(CUT_AMOUNT)

	res = Image.fromarray(np.array(img).astype(np.uint8), original.mode)

	res.save("carved" + str(CUT_AMOUNT) + ".png")
	res.show()



















