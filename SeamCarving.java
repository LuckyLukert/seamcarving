import javax.imageio.ImageIO;
import java.awt.image.*;
import java.awt.*;
import java.io.IOException;
import java.io.File;
import java.util.Arrays;
import static java.lang.Math.*;

public class SeamCarving {
    static BufferedImage img;  // the image being edited
    static double[][] dp;  // the dp array for finding the best seam

    static int h() { return img.getHeight(); }
    static int w() { return img.getWidth(); }

    // calculate the euclidean distance between two colors
    static double dist(int color1, int color2) {
        // use new Color(color1)  //from awt
        return 0;
    }

    // calculate the dynamic programming array
    static void calcDP() {
        //use img.getRGB(x, y)
    }

    // a list of x-indices which are the seam with the least contrast
    static int[] getSeam(){
        return null;
    }

    // remove the seam from the image
    static void carveSeam(int[] seam){
        
    }

    // remove cutAmount seams from the image
    static void carve(int cutAmount){
        // use img.setRGB(x, y, color)
        // use img.getSubimage(x1, y1, x2, y2)
    }

    static void loadImage(String fileName){
        try {
            img = ImageIO.read(new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void saveImage(String fileName){
        try {
            ImageIO.write(img, "png", new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        int carveAmount = 50;
        String fileName = "paris.png";

        loadImage(fileName);
        carve(carveAmount);
        saveImage("carved" + carveAmount + ".png");
    }
}